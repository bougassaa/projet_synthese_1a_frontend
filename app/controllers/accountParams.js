class AccountParamsController extends BaseController {
    constructor() {
        super();

        this.initializePage("Paramètre du compte");
        setActiveLink(false);

        this.user = null;
        this.fillUserValues();
    }

    async fillUserValues() {
        let rep = await this.modelUser.getUser();
        this.user = Object.assign(new User(),rep);

        $("#editName").value = this.user.name;
        $("#editEmail").value = this.user.email;
        $("#editPassword").value = this.user.password;
    }

    prepareEditInfos() {
        $("#btPrepareEditInfos").classList.add("d-none");
        $(".form-control", item => {
            item.removeAttribute('readonly');
        });
    }

    checkIfValuesChanged() {
        if ($("#editName").value !== this.user.name ||
            $("#editEmail").value !== this.user.email ||
            $("#editPassword").value !== this.user.password) {
            $("#btSaveEdit").classList.remove("d-none");
        } else {
            $("#btSaveEdit").classList.add("d-none");
        }
    }

    saveChanges() {
        let nameInput = $("#editName");
        let emailInput = $("#editEmail");
        let passwordInput = $("#editPassword");

        $(".form-control", item => {
            item.classList.remove("is-invalid");
        });

        if(nameInput.value.length < 3) {
            nameInput.classList.add("is-invalid");
            this.toast("Le nom d'utilisateur est trop court.");
            return;
        }

        if(emailInput.value.length === 0) {
            emailInput.classList.add("is-invalid");
            this.toast();
            return;
        }

        if(passwordInput.value.length === 0) {
            passwordInput.classList.add("is-invalid");
            this.toast();
            return;
        }

        if(!validateEmail(emailInput.value)) {
            emailInput.classList.add("is-invalid");
            this.toast("Adresse e-mail invalide");
            return;
        }

        this.user.name = nameInput.value;
        this.user.email = emailInput.value;
        this.user.password = passwordInput.value;

        this.modelUser.update(this.user).then((res) => {
            if(res.status === 200) {
                let alert = $("#successEdit");

                alert.classList.remove("d-none");

                res.json().then(rep => {
                    sessionStorage.setItem("token", rep.token);
                    setTimeout(_ => window.location.replace("index.html"), 1000);
                });
            } else {
                this.toast("Un problème serveur est survenue, réessayer dans quelques instant.")
            }
        })
    }
}

window.accountParamsController = new AccountParamsController();