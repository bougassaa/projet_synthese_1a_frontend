class AdminController extends BaseController {
    constructor() {
        super();

        this.initializePage("Panel administrateur");

        this.isAdmin().then(isAdmin => {
            if (!isAdmin) {
                navigate("index");
            }
        });

        this.idUserChangeMail = null;
        this.idUserSendNotification = null;
        this.searchUserForAdmin("");
        setActiveLink(null);
    }

    async searchUserForAdmin(value) {
        $("#noUsers").classList.add("d-none");
        $("#allUsers").classList.add("d-none");
        jQuery(".t-tip").tooltip('hide');

        let users = await this.modelUser.getUsersFromTyping(value);

        if(users.length > 0) {
            $("#allUsers").classList.remove("d-none");

            let content = "";
            for (let user of users) {
                content += `<tr>
                                <td>${user.name}</td>
                                <td>${user.email}</td>
                                <td>
                                    <span class="text-${user.admin ? 'success' : 'danger'}">
                                        ${user.admin ? 'Oui' : 'Non'}
                                    </span>
                                </td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-primary btn-sm mr-1 t-tip" onclick="adminController.displayModalChangeEmail(${user.id})" data-toggle="hover" title="Modifier l'adresse e-mail de cet utilisateur.">
                                        <i class="fas fa-at"></i>
                                    </button>
                                    <button type="button" class="btn btn-warning btn-sm mr-1 t-tip" onclick="adminController.sendLinkToChangePassword('${user.email}')" data-toggle="hover" title="Envoyer un e-mail a cet utlisateur pour qu'il change son mot de passe.">
                                        <i class="fas fa-key"></i>
                                    </button>
                                    <button type="button" class="btn btn-secondary btn-sm mr-1 t-tip" onclick="adminController.displayModalSendNotification(${user.id})" data-toggle="hover" title="Envoyer une notification à cet utilisateur.">
                                        <i class="far fa-comment-alt"></i>
                                    </button>
                                    
                                    <button type="button" class="btn btn-${user.subscriber ? 'danger' : 'success'} btn-sm mr-1 t-tip" data-toggle="hover" 
                                    onclick="adminController.updateUser('subscriber', ${!user.subscriber}, ${user.id})"
                                    title="${user.subscriber ? 'Désabonner cette personne.' : 'Abonner cette personne gratuitement.'}">
                                        <i class="fas fa-shopping-cart"></i>
                                    </button>
                                    
                                    <button type="button" class="btn btn-${user.blocked ? 'success' : 'danger'} btn-sm mr-1 t-tip" data-toggle="hover" 
                                    onclick="adminController.updateUser('blocked', ${!user.blocked}, ${user.id})"
                                    title="${user.blocked ? 'Débloquer l\'accès a cette personne.' : 'Bloquer l\'accès a cette personne.'}">
                                        <i class="fas fa-${user.blocked ? 'unlock' : 'lock'}"></i>
                                    </button>
                                    
                                    <button type="button" class="btn btn-${user.admin ? 'danger' : 'success'} btn-sm mr-1 t-tip" data-toggle="hover" 
                                    onclick="adminController.updateUser('admin', ${!user.admin}, ${user.id})"
                                    title="${user.admin ? 'Enlever le rôle administrateur à cet utilisateur.' : 'Donner le rôle administrateur à cet utilisateur.'}">
                                        <i class="fas fa-${user.admin ? 'user-slash' : 'user-shield'}"></i>
                                    </button>
                                </td>
                            </tr>`;
            }

            $("#allUsers tbody").innerHTML = content;
            jQuery(".t-tip").tooltip({ trigger: "hover" });
        } else {
            $("#noUsers").classList.remove("d-none");
        }

    }

    displayModalChangeEmail(id) {
        $("#newEmail").value = "";
        $("#newEmail").classList.remove("is-invalid");
        jQuery('#changeEmail').modal('show');
        adminController.idUserChangeMail = id;
    }

    async changeEmail() {
        if(this.idUserChangeMail !== null) {
            let email = $("#newEmail").value;

            if(validateEmail(email)) {
                jQuery('#changeEmail').modal('hide');

                this.modelUser.changeEmail(this.idUserChangeMail, email)
                    .then(_ => this.searchUserForAdmin(""))
                    .catch(_ => this.toast("Une erreur s'est produite"))
            } else {
                $("#newEmail").classList.add("is-invalid");
            }
        } else {
            jQuery('#changeEmail').modal('hide');
            this.toast("Veuillez actualiser la page et réessayer.")
        }
    }

    sendLinkToChangePassword(email) {
        this.modelUser.api.newLinkToChangePassword(email)
            .then(_ => this.success("Lien envoyé à l'utilisateur"))
            .catch(_ => {
                this.toast("Envoie du lien impossible, erreur serveur, réessayer plus tard.");
            });
    }

    async updateUser(field, value, id) {
        let user = await this.modelUser.getById(id);
        user[field] = value;

        this.modelUser.updateFromAdmin(user).then(rep => {
            this.searchUserForAdmin("");
        })
    }

    async getSubscriber() {
        let subscribers = await this.modelUser.getSubscriber();

        if(subscribers.length > 0) {
            let content = "";
            for (let user of subscribers) {
                content += `<tr>
                                <td>${user.name}</td>
                                <td>${user.email}</td>
                            </tr>`;
            }

            $("#allSubscriber tbody").innerHTML = content;
            $("#allSubscriber").classList.remove("d-none");
        } else {
            $("#noSubscriber").classList.remove("d-none");
        }
    }

    displayModalSendNotification(userId) {
        $("#notificationTitle").value = "";
        $("#notificationTitle").classList.remove("is-invalid");

        $("#notificationMessage").value = "";
        $("#notificationMessage").classList.remove("is-invalid");

        jQuery('#writeNotification').modal('show');
        this.idUserSendNotification = userId;
    }

    sendNotification() {
        if(this.idUserSendNotification !== null) {
            let title = $("#notificationTitle").value;
            let message = $("#notificationMessage").value;

            if(title.trim().length && message.trim().length) {
                jQuery('#writeNotification').modal('hide');

                let notifApi = new NotificationApi()

                notifApi.sendNotification(this.idUserSendNotification, title, message)
                    .then(_ => this.success("Notification envoyée"))
                    .catch(_ => this.toast("Une erreur s'est produite"))
            } else {
                $("#notificationTitle").classList.add("is-invalid");
                $("#notificationMessage").classList.add("is-invalid");
            }
        } else {
            jQuery('#writeNotification').modal('hide');
            this.toast("Veuillez actualiser la page et réessayer.")
        }
    }
}

window.adminController = new AdminController();