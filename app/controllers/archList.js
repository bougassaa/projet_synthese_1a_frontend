class ArchListController extends BaseController {
    constructor() {
        super();

        this.initializePage("Listes archivées");
        setActiveLink('#navLinkArchivList');
        this.displayArchList();
    }

    async displayArchList() {
        let records = await this.modelList.getArchivedList();
        let content = "";
        if (records.length > 0) {
            $("#archListContainer").classList.remove("d-none");

            for (let list of records) {
                let items = await this.modelItem.getItemOfList(list.id);

                content += `<tr id="tr_${list.id}">
                            <td>${list.shop}</td>
                            <td>${list.date}</td>
                            <td>${items.length}</td>
                            <td class="text-right">
                                <button type="button" class="btn btn-primary btn-sm mr-1" onclick="archListController.openModal('${list.shop.replace(/'/gi, '')}', ${list.id})">
                                    <i class="far fa-eye"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-sm" onclick="archListController.deleteList(${list.id})">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>`;
            }

            $("#archListContainer tbody").innerHTML = content;
        } else {
            $("#archListContainer").classList.add("d-none");
            $("#archListEmpty").classList.remove("d-none");
        }

    }

    async checkItem(id, listId) {
        let item = await this.modelItem.getById(id);
        item.checked = 1;
        await this.modelItem.update(item);
        this.displayArchCurrentList(listId)
    }

    async displayArchCurrentList(listId) {
        let records = await this.modelItem.getItemOfList(listId);

        let content = "";
        for (let Item of records) {
            content += `<li class="list-group-item row no-gutters d-flex mx-0 align-items-center ${Item.checked == 1 ? 'list-group-item-success' : ''}">
                        <div class="col-2 col-sm-1 ">
                            <button type="button" class="btn btn-outline-success ${Item.checked == 1 ? 'd-none' : ''}" onclick="archListController.checkItem(${Item.id}, ${listId})">
                                <i class="fas fa-tasks"></i>
                            </button>
                        </div>
                        <div class="col-1 col-sm-1 font-weight-bold">${Item.quantity}</div>
                        <div class="col-7 col-sm-9">${Item.label}</div>
                    </li>`;
        }

        $(".listItem").innerHTML = content;
    }

    openModal(nom, id) {
        $("#createListModal .modal-title").innerText = nom;
        jQuery("#createListModal").modal("show");
        this.displayArchCurrentList(id);
        $("#formNewList").classList.add("d-none");
    }
}

window.archListController = new ArchListController();
