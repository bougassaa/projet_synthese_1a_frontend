class BaseController {
    constructor(fromLogin = false) {
        if(fromLogin === false) {
            this.checkAuthentication();
            this.displayNotification();

            jQuery('#listNotification').click(function(e) {
                e.stopPropagation();
            });
        }
        this.setBackButtonView('index');

        this.model = new Model();
        this.modelItem = new ModelItem();
        this.modelList = new ModelList();
        this.modelUser = new ModelUser();
        this.modelShare = new ModelShare();
        this.listId = null;
        this.currentItemEdition = null;
        this.currentListEdition = null;

        this.removeLinkIfNonAdmin();

        jQuery(".t-tip").tooltip();
    }

    initializePage(name) {
        $("#titleNavBar").innerText = name;
        document.title = name;
    }

    toast(msg = "Ne pas laisser les champs vide.") {
        $("#toastDanger .toast-body").innerText = msg;
        jQuery("#toastDanger").toast("show");
    }

    success(msg) {
        $("#toastSuccess .toast-body").innerText = msg;
        jQuery("#toastSuccess").toast("show");
    }

    setBackButtonView(view) {
        window.onpopstate = function() {
            navigate(view)
        }; history.pushState({}, '');
    }

    async addNewItem() {
        const inputNom = $("#inputNom");
        const inputQte = $("#inputQte");

        let nom = inputNom.value;
        let qte = inputQte.value;

        if(nom.trim().length === 0) {
            inputNom.classList.add("is-invalid");
            this.toast();
        } else if(this.listId === null) {
            $("#inputListName").classList.add("is-invalid");
            this.toast("Avant d'ajouter un Item veuillez renseigner le nom de la liste, merci.");
        } else {
            inputNom.classList.remove("is-invalid");
            await this.modelItem.save(new Item(null, nom, qte,0, this.listId));
            await this.displayCurrentList(this.listId);

            if($("#listContainer")) {
                await indexController.displayList();
            }

            inputNom.value = "";
            inputQte.value = 1;
            inputQte.focus();
        }
    }

    deleteList(id) {
        let modal = jQuery("#confirmModal").modal("show");
        let instance = this;
        $("#confirmDeleteList").onclick = async function () {
            await instance.modelList.delete(id);

            if($("#listContainer")) {
                await indexController.displayList();
            } else if($("#archListContainer")) {
                await archListController.displayArchList()
            }

            modal.modal("hide");
        };
    }

    async displayCurrentList(listId) {
        let records = await this.modelItem.getItemOfList(listId);

        let content = "";
        for (let Item of records) {
            content += `<li class="list-group-item row no-gutters d-flex mx-0 align-items-center ${Item.checked == 1 ? 'list-group-item-success' : ''}">
                        <div class="col-2 col-sm-1 ">
                            <button type="button" class="btn btn-outline-success ${Item.checked == 1 ? 'd-none' : ''}" onclick="indexController.checkItem(${Item.id}, ${listId})">
                                <i class="fas fa-tasks"></i>
                            </button>
                        </div>
                        <div class="col-1 col-sm-1 font-weight-bold">${Item.quantity}</div>
                        <div class="col-7 col-sm-9">${Item.label}</div>
                        <div class="col-2 col-sm-1 text-right controlButtonModal">
                            <button type="button" class="btn btn-outline-primary ${Item.checked == 1 ? 'd-none' : ''}" onclick="indexController.prepareEditItem(${Item.id}, ${listId})">
                                <i class="far fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-outline-danger" onclick="indexController.deleteItem(${Item.id}, ${listId})">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </div>
                    </li>`;
        }

        $(".listItem").innerHTML = content;
    }

    async deleteItem(id, listId) {
        await this.modelItem.delete(id);
        this.displayCurrentList(listId);

        if($("#listContainer")) {
            await indexController.displayList();
        } else if($("#archListContainer")) {
            await archListController.displayArchList()
        }
    }

    async prepareEditItem(id, listId) {
        let item = await this.modelItem.getById(id);
        const inputNom = $("#inputNom");

        inputNom.value = item.label;
        inputNom.focus();
        inputNom.dataset.forEdit = "1";
        $("#inputQte").value = item.quantity;
        $("#addNewItem").classList.add("d-none");
        $("#editItem").classList.remove("d-none");

        this.currentItemEdition = id;
        this.currentListEdition = listId;
    }

    async editItem() {
        let item = await this.modelItem.getById(this.currentItemEdition);
        const inputNom = $("#inputNom");
        const inputQte = $("#inputQte");

        let nom = inputNom.value;
        let qte = inputQte.value;

        if(nom.trim().length === 0) {
            inputNom.classList.add("is-invalid");
            this.toast();
        } else {
            inputNom.classList.remove("is-invalid");
            item.quantity = qte;
            item.label = nom;
            await this.modelItem.update(item);
            await this.displayCurrentList(this.currentListEdition);

            if($("#listContainer")) {
                await indexController.displayList();
            }

            inputNom.value = "";
            inputQte.value = 1;
            inputQte.focus();
            $("#addNewItem").classList.remove("d-none");
            $("#editItem").classList.add("d-none");
            inputNom.dataset.forEdit = "0";
        }
    }

    checkAuthentication() {
        if ([null, ""].includes(sessionStorage.getItem("token"))) {
            // todo : check if token is available
            window.location.replace("login.html")
        }
    }

    async isAdmin() {
        if(!sessionStorage.getItem("token")) return;

        let rep = await this.modelUser.getUser();
        return rep.admin;
    }

    removeLinkIfNonAdmin() {
        this.isAdmin().then(isAdmin => {
            if (!isAdmin) {
                $(".noneForNonAdmin", item => {
                    item.classList.add("d-none");
                });
            }
        });
    }

    modal(title, body) {
        $("#genericModal .modal-title").innerText = title;
        $("#genericModal .modal-body").innerHTML = body;

        jQuery("#genericModal").modal("show");
    }

    closeModal() {
        jQuery("#genericModal").modal("show");
    }

    async checkIfSubscriber(msg) {
        let rep = await this.modelUser.api.checkIfSubscriber();

        if(!rep.subscriber){
            this.modal(
                "Abonnez vous 🙂",
                msg + '<br>Cliquez <a href="javascript:indexController.displaySubscribeModal();">ici</a> pour vous abonner.'
            );
        }

        return rep.subscriber;
    }

    async navigateToSharelist() {
        if(await this.checkIfSubscriber("Pour pouvoir utiliser les partages de listes, vous devez être abonné.")) {
            navigate("shareList");
        }
    }

    subscribe(form) {
        jQuery("#subscribeModal").modal("hide");
        this.displayLoader();

        this.modelUser.api.subscribe(formToUrl(form))
            .then(_ => {
                this.hideLoader();
                this.modal(
                    "Vous êtes abonné ✅",
                    'Félicitation, vous êtes abonné, vous pouvez utiliser toutes les fonctionnalitées.'
                );
            })
            .catch(_ => {
                this.hideLoader();
                this.toast("Problème serveur")
            });

        return false;
    }

    displayLoader() {
        $("#loader").style.display = "flex";
    }

    hideLoader() {
        $("#loader").style.display = "none";
    }

    displaySubscribeModal() {
        let modal = `<div class="modal fade text-body" tabindex="-1" role="dialog" id="subscribeModal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Abonnement</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            L'abonnement au site est de <strong>13,99 € / mois</strong> sans engagement. Vous aurez accès à toutes les fonctionnalités du site.            
                        </div>
                        <form onsubmit="return indexController.subscribe(this)" id="subscribe-form" class="my-2">
                            <p>
                                Saisir les informations de votre carte bancaire pour procéder au paiement:
                            </p>
                            <div class="form-group">
                                <label for="subscribe-card-name">Nom du titulaire de la carte</label>
                                <input required type="text" class="form-control" id="subscribe-card-name" placeholder="Jean Dupont" name="cardowner">
                            </div>
                            <div class="form-group">
                                <label for="subscribe-card-number">Numéro de la carte</label>
                                <input required type="text" pattern="[0-9]{16}" maxlength="16" class="form-control" id="subscribe-card-number" placeholder="8578 7594 5830 2930" name="cardnumber">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label for="subscribe-card-cvc">Numéro de sécurité</label>
                                    <input type="text" pattern="[0-9]{3}" maxlength="3" class="form-control" id="subscribe-card-cvc" placeholder="451" required name="cardsecurity">
                                </div>
                                <div class="col">
                                    <label for="subscribe-card-date">Date d'expiration</label>
                                    <input type="text" pattern="(0[1-9]|1[0-2])/[0-9]{2}" maxlength="5" class="form-control" id="subscribe-card-date" placeholder="MM/YY" required name="carddate">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                        <button type="submit" form="subscribe-form" class="btn btn-primary">Payer !</button>
                    </div>
                </div>
            </div>
        </div>`;

        jQuery("#subscribeModal").remove();
        jQuery(".modal").modal("hide");
        jQuery(modal).modal("show");
    }

    async displayNotification() {
        let notifApi = new NotificationApi()

        let unread = await notifApi.getUnreadNotification()

        let countUnread = unread.length;

        if(countUnread) {
            $("#notificationCounter").innerText = countUnread
        } else {
            $("#notificationCounter").classList.add("d-none");
        }

        let notifications = await notifApi.getAll()

        let content = ""

        for (let notif of notifications) {
            content += `<li class="dropdown-item px-2" ${notif.seen ?'':'style="background:#efefef"'}>
                    <div style="display: flex;align-items: center;">
                        <div style="white-space: normal;flex-grow: 1;">
                            <strong>${notif.title}</strong><br>${notif.message}
                        </div>
                        <div>
                            <button type="button" class="btn btn-outline-success btn-sm t-tip ${notif.seen ?'d-none':''}" onclick="indexController.checkNotification(${notif.id})" data-toggle="hover" title="Marquer comme lu.">
                                <i class="fas fa-check"></i>
                            </button>
                        </div>
                    </div>
                </li>`
        }

        jQuery(".t-tip").tooltip('hide');

        if(content !== ""){
            $("#listNotification").innerHTML = content
        } else {
            $("#msgEmptyNotification").classList.remove("d-none");
        }

        jQuery(".t-tip").tooltip({ trigger: "hover" });
    }

    async checkNotification(id) {
        let notifApi = new NotificationApi()
        await notifApi.checkNotification(id)
        await this.displayNotification()
    }
}
