class IndexController extends BaseController {
    constructor() {
        super();

        this.initializePage("Toutes les listes");
        setActiveLink('#navLinkCurrentList');

        this.displayList();
        this.initializeForSharing();
    }

    initializeForSharing() {
        this.sharingList = null;
        this.sharingUser = null;
    }

    async checkItem(id, listId) {
        let item = await this.modelItem.getById(id);
        item.checked = 1;
        await this.modelItem.update(item);
        this.displayCurrentList(listId)
    }

    async displayList() {
        let records = await this.modelList.getAllList();

        let content = "";
        if (records.length > 0) {
            $("#listContainer").classList.remove("d-none");
            for (let list of records) {
                let items = await this.modelItem.getItemOfList(list.id);

                content += `<tr id="tr_${list.id}">
                                <td>${list.shop}</td>
                                <td>${list.date}</td>
                                <td>${items.length}</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-primary btn-sm mr-1 t-tip" onclick="indexController.openEditModal('${list.shop.replace(/'/gi, '')}', ${list.id})" data-toggle="hover" title="Modifier la liste ou/et ajouter des éléments.">
                                        <i class="far fa-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-sm mr-1 t-tip" onclick="indexController.openShareModal(${list.id})" data-toggle="hover" title="Partager cette liste avec des utilisateurs.">
                                        <i class="fas fa-share-alt"></i>
                                    </button>
                                    <button type="button" class="btn btn-info btn-sm mr-1 t-tip" onclick="indexController.archiveList(${list.id})" data-toggle="hover" title="Archiver la liste.">
                                        <i class="fas fa-archive"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-sm t-tip" onclick="indexController.deleteList(${list.id})" data-toggle="hover" title="Supprimer la liste.">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>`;
            }

            $("#listContainer tbody").innerHTML = content;
            jQuery(".t-tip").tooltip();
        } else {
            $("#listContainer").classList.add("d-none");
            $("#currentListEmpty").classList.remove("d-none");
        }

    }

    openEditModal(listName, id) {
        $("#inputQte").value = 1;
        $("#inputNom").value = "";
        $("#createListModal .modal-title").innerText = listName;
        jQuery("#createListModal").modal("show");
        this.listId = id;
        this.displayCurrentList(id);
        $("#formNewList").classList.remove("d-none");
    }

    async archiveList(id) {
        let list = await this.modelList.getById(id);
        list.archived = 1;
        await this.modelList.update(list);
        this.displayList();
        jQuery(".t-tip").tooltip("hide");
    }

    deconnect() {
        sessionStorage.setItem("token", "");
        window.location.replace("login.html");
    }

    async openShareModal(listId) {
        if(!await this.checkIfSubscriber("Pour pouvoir utiliser les partages de listes, vous devez être abonné.")) return false;

        this.initializeForSharing();
        this.sharingList = listId;

        $("#formGroupSharing").classList.remove("d-none");
        $("#nextStepSharing").classList.add("d-none");
        $("#formGroupSharing input").value = "";
        $("#searchUserResult").innerHTML = "";
        $("#cbEditList").checked = false;
        $("#btSaveSharing").disabled = true;

        jQuery("#shareModal").modal("show");
    }

    async searchUser(input) {
        const name = input.value.trim();

        if(name.length >= 3) {
            let users = await this.modelUser.getUsersFromTyping(name);

            let list = $("#searchUserResult");

            if(users.length > 0) {
                let content = "";
                for (let user of users) {
                    content += `<button type="button" class="list-group-item list-group-item-action" onclick="indexController.chooseUserForSharing(${user.id}, '${user.name}')">${user.name}</button>`;
                }
                list.innerHTML = content;
            } else {
                list.innerHTML = "Aucun résultat.";
            }
        }
    }

    chooseUserForSharing(id, name) {
        this.sharingUser = id;

        $("#formGroupSharing").classList.add("d-none");
        $("#nextStepSharing").classList.remove("d-none");
        $("#userNameSharing").innerHTML = `Utilisateur choisis : <b>${name}</b>`;
        $("#btSaveSharing").disabled = false;
    }

    async saveSharing() {
        await this.modelShare.share(this.sharingList, this.sharingUser, $("#cbEditList").checked);

        jQuery("#shareModal").modal("hide");

        if($("#shareFromMe")) {
            shareListController.displaySharedListFromUser();
        }
    }
}

window.indexController = new IndexController();
