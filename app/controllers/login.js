class LoginController extends BaseController {
    constructor() {
        super(true);
        this.svc = new UserApi();
        this.checkForActivation();
        this.checkForChangePassword();
    }
    async create() {
        let email = $("#newEmail").value;
        let name = $("#newName").value;
        let password = $("#newPassword").value;

        if(email.length === 0 || password.length === 0) {
            this.toast();
            return;
        }

        if(!validateEmail(email)) {
            $("#newEmail").classList.add("is-invalid");
            this.toast("Saisir un format correct pour l'adresse e-mail.");
            return;
        } else {
            $("#newEmail").classList.remove("is-invalid");
        }

        if(name.length < 3) {
            $("#newName").classList.add("is-invalid");
            this.toast("Le nom est trop court.");
            return;
        } else {
            $("#newName").classList.remove("is-invalid");
        }

        this.displayLoader();
        this.svc.create(email, name, password)
            .then(res => {
                this.displayActivationModal(
                    "Activation du compte",
                    `Pour pouvoir accéder au site, vous devez activer votre compte, depuis l'e-mail envoyé sur cette adresse : <b>${email}</b>. <br><small class="form-text text-muted">Le lien d'activation a une durée limitée de 24 heures.</small>  `
                );

                $("#newEmail").value = "";
                $("#newName").value = "";
                $("#newPassword").value = "";

                this.hideLoader();
            })
            .catch(err => {
                if (err == 401) {
                    $("#createError").classList.remove("d-none");
                } else {
                    this.toast("Service injoignable ou problème réseau");
                }
                this.hideLoader();
            })
    }

    authenticate() {
        let email = $("#email").value;
        let password = $("#password").value;

        if(email.length === 0 || password.length === 0) {
            this.toast();
            return;
        }

        if(!validateEmail(email)) {
            $("#email").classList.add("is-invalid");
            this.toast("Saisir un format correct pour l'adresse e-mail.");
            return;
        } else {
            $("#email").classList.remove("is-invalid");
        }

        this.displayLoader();
        this.svc.authenticate(email, password)
            .then(res => {
                this.hideLoader();
                sessionStorage.setItem("token", res.token);
                window.location.replace("index.html");
            })
            .catch(err => {
                this.hideLoader();
                console.log(err);
                if (err == 401) {
                    $("#logError").classList.remove("d-none");
                } else if (err == 403) {
                    this.displayActivationModal(
                        "Compte non activé",
                        `Votre compte n'a pas encore été activé, un e-mail vous a été envoyé, sa durée est limitée à 24 heures. Si vous souhaitez en regénérer un nouveau, cliquez <a href="javascript:loginController.generateLinkFromEmail('${email}');">ici</a>.`
                    );
                } else if (err == 402) {
                    this.displayActivationModal(
                        "Compte bloqué",
                        `Bonjour, votre compte a été bloqué par un administrateur du site.`
                    );
                } else {
                    this.toast("Service injoignable ou problème réseau");
                }
            })
    }

    displayCreateUserForm() {
        $("#cardLogin").classList.add("d-none");
        $("#cardCreateUser").classList.remove("d-none");
    }

    checkForActivation() {
        let parsedUrl = new URL(window.location.href);
        let active = parsedUrl.searchParams.get("active");

        if(active !== null) {
            this.svc.checkForActivation(active)
                .then(res => {
                    this.displayActivationModal(
                        "Activation réuissi",
                        "Félicitation, votre compte a été activé, vous serez redirigé sur le site dans quelques secondes."
                    );
                    setTimeout(_ => {
                        sessionStorage.setItem("token", res.token);
                        window.location.replace("index.html");
                    }, 3000)
                })
                .catch(_ => {
                    this.displayActivationModal(
                        "Activation échouée",
                        `L'activation n'a pas réuissi, car le lien n'est plus valable, cliquez <a href="javascript:loginController.generateLinkFromIdActivation(${active});">ici</a> pour recevoir un nouveau lien.`
                    );
                })
        }
    }

    displayActivationModal(title, body) {
        $("#btSendNewLink").classList.add("d-none");
        $("#btSendLinkPassword").classList.add("d-none");
        $("#btChangePassword").classList.add("d-none");

        $("#activeEmailModal .modal-title").innerText = title;
        $("#activeEmailModal .modal-body").innerHTML = body;

        jQuery("#activeEmailModal").modal("show");
    }

    generateLinkFromIdActivation(id) {
        jQuery("#activeEmailModal").modal("hide");
        let instance = this;
        this.svc.newActivationFromIdActivation(id)
            .then(res => {
                instance.displayActivationModal(
                    "Activation envoyée",
                    `Le lien de l'activation a été renvoyé sur cette adresse e-mail : <b>${res.email}</b>. <br><small class="form-text text-muted">Le lien d'activation a une durée limitée de 24 heures.</small>`
                );
            })
            .catch(_ => {
                this.toast("Envoie de l'email d'activation impossible, il est possible que votre compte sois déjà activé, ou sinon que vous n'avez pas encore créer de compte");
            });
    }

    generateLinkFromEmail(email) {
        jQuery("#activeEmailModal").modal("hide");
        let instance = this;
        this.svc.newActivationFromEmail(email)
            .then(res => {
                instance.displayActivationModal(
                    "Activation envoyée",
                    `Le lien de l'activation a été renvoyé sur cette adresse e-mail : <b>${res.email}</b>. <br><small class="form-text text-muted">Le lien d'activation a une durée limitée de 24 heures.</small>`
                );
            })
            .catch(_ => {
                this.toast("Envoie de l'email d'activation impossible, il est possible que votre compte sois déjà activé, ou sinon que vous n'avez pas encore créer de compte");
            });
    }

    sendActivationLinkFromModal() {
        let email = $("#emailForNewLink").value;
        if(email.length !== 0 && validateEmail(email)) {
            this.generateLinkFromEmail(email);
        } else {
            $("#emailForNewLink").classList.add("is-invalid");
        }
    }

    displayModalNewLink() {
        this.displayActivationModal(
            "Envoi d'un nouveau lien d'activation",
            `Saisir votre adresse e-mail pour recevoir un nouveau lien d'activation :<div class="form-group mt-2"><input type="text" class="form-control" id="emailForNewLink" placeholder="Adresse e-mail ..."></div>`
        );

        $("#btSendNewLink").classList.remove("d-none");

        let input = $("#emailForNewLink");
        input.classList.remove("is-invalid");

        let email = $("#email").value;

        if(email.length !== 0 && validateEmail(email)) {
            input.value = email;
        }
    }

    displayModalChangePassword() {
        this.displayActivationModal(
            "Envoi du lien pour changer le mot de passe",
            `Saisir votre adresse e-mail pour recevoir un lien à fin de changer votre mot de passe :<div class="form-group mt-2"><input type="text" class="form-control" id="emailForNewLink" placeholder="Adresse e-mail ..."></div>`
        );

        $("#btSendLinkPassword").classList.remove("d-none");

        let input = $("#emailForNewLink");
        input.classList.remove("is-invalid");

        let email = $("#email").value;

        if(email.length !== 0 && validateEmail(email)) {
            input.value = email;
        }
    }

    sendLinkToChangePassword() {
        let email = $("#emailForNewLink").value;
        jQuery("#activeEmailModal").modal("hide");
        let instance = this;

        if(email.length !== 0 && validateEmail(email)) {
            this.displayLoader();
            this.svc.newLinkToChangePassword(email)
                .then(res => {
                    this.hideLoader();
                    instance.displayActivationModal(
                        "Lien envoyé",
                        `Le lien pour changer votre mot de passe a été envoyé sur cette adresse e-mail : <b>${res.email}</b>. <br><small class="form-text text-muted">Le lien a une durée limitée de 30 minutes.</small>`
                    );
                })
                .catch(_ => {
                    this.hideLoader();
                    this.toast("Envoie du lien impossible, adresse e-mail erronée.");
                });
        } else {
            $("#emailForNewLink").classList.add("is-invalid");
        }
    }

    checkForChangePassword() {
        let parsedUrl = new URL(window.location.href);
        let id = parsedUrl.searchParams.get("newPwd");

        if(id !== null) {
            this.svc.checkForChangePassword(id)
                .then(_ => {
                    this.displayActivationModal(
                        "Changement du mot de passe",
                        `Saisir le nouveau mot de passe :<div class="form-group mt-2"><input type="password" data-permanent_link="${id}" class="form-control" id="changePassword" placeholder="Nouveau mot de passe ..."></div>`
                    );
                    $("#btChangePassword").classList.remove("d-none");
                })
                .catch(_ => {
                    this.toast("Le lien n'es plus actif, appuyez sur mot de passe oublié pour reéssayer.")
                })
        }
    }

    changePassword() {
        let input = $("#changePassword");
        let pwd = input.value;

        if(pwd.length === 0) {
            input.classList.add("is-invalid");
        } else {
            this.svc.changePassword(pwd, input.dataset.permanent_link)
                .then(rep => {
                    if(rep.token == null) {
                        this.displayActivationModal(
                            "Mot de passe changé !",
                            "Votre mot de passe a bien été changé, pour accéder au site vous devez activé votre compte avec l'e-mail reçu, sinon vous pouvez refaire une demande d'activation du compte."
                        );
                    } else {
                        this.displayActivationModal(
                            "Mot de passe changé !",
                            "Votre mot de passe a bien été changé, vous serez redirigé sur le site dans quelques secondes."
                        );
                        setTimeout(_ => {
                            sessionStorage.setItem("token", rep.token);
                            window.location.replace("index.html");
                        }, 3000)
                    }
                })
                .catch(_ => {
                    this.toast("Un problème serveur est survenue, veuillez renouveler votre demande merci.")
                })
        }
    }
}

window.loginController = new LoginController();