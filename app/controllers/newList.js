class NewListController extends BaseController {
    constructor() {
        super();

        this.initializePage("Nouvelle liste");
        setActiveLink('#navLinkNewList');
        $("#inputQte").focus();
    }

    async addNameList(input) {
        let rep = await this.modelList.getNoOfCurrentList();

        if(rep.noOfCurrentList >= 1) {
            if(!await this.checkIfSubscriber("Vous êtes limité a 1 liste en cours, pour pouvoir en créer plusieurs en cours, vous devez être abonné.")) {
                return;
            }
        }

        let listName = input.value;

        if(listName.trim().length === 0) {
            input.classList.add("is-invalid");
            this.toast();
        } else {
            $(".listeName").innerText = listName;
            input.classList.add("d-none");

            this.listId = await this.modelList.save(new List(null, listName, new Date().toLocaleDateString(), 0));
        }
    }
}

window.newListController = new NewListController();