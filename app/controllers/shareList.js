class ShareListController extends BaseController {
    constructor() {
        super();

        this.initializePage("Partage de liste");
        setActiveLink('#navLinkShareList');

        this.displaySharedListFromUser();
        this.displaySharedListForOthers();
    }


    async displaySharedListFromUser() {
        let records = await this.modelShare.listSharedFromUser();

        let content = "";
        if (records.length > 0) {
            $("#shareFromMe table").classList.remove("d-none");
            for (let list of records) {
                content += `<tr id="tr_${list.id}">
                                <td>${list.shop}</td>
                                <td>${new Date(list.date).toLocaleDateString()}</td>
                                <td>${list.noOfShare}</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-primary btn-sm mr-1 t-tip" onclick="indexController.openShareModal(${list.id})" data-toggle="hover" title="Partager à d'autres personnes, ou modifier les droits des personnes actuelles.">
                                        <i class="fas fa-share-alt"></i>
                                    </button>
                                </td>
                            </tr>`;
            }

            $("#shareFromMe table tbody").innerHTML = content;
            jQuery(".t-tip").tooltip();
        } else {
            $("#shareFromMe table").classList.add("d-none");
            $("#shareFromMe h5").classList.remove("d-none");
        }
    }

    async displaySharedListForOthers() {
        let records = await this.modelShare.listSharedForOthers();

        let content = "";
        if (records.length > 0) {
            $("#shareForMe table").classList.remove("d-none");
            for (let list of records) {
                content += `<tr id="tr_${list.id}">
                                <td>${list.shop}</td>
                                <td>${list.name}</td>
                                <td>${new Date(list.date).toLocaleDateString()}</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-primary btn-sm t-tip ${list.editable === 1 ? 'd-none' : ''}" onclick="shareListController.openViewItemsModal('${list.shop.replace(/'/gi, '')}', ${list.id})" data-toggle="hover" title="Vous avez seulement le droit de visualiser la liste.">
                                        <i class="far fa-eye"></i>
                                    </button>
                                    <button type="button" class="${list.archived ? 'disabled' : ''} btn btn-primary btn-sm t-tip ${list.editable === 0 ? 'd-none' : ''}" 
                                            ${list.archived ? '' : `onclick="indexController.openEditModal('${list.shop.replace(/'/gi, '')}', ${list.id})"`} data-toggle="hover" 
                                            ${list.archived ? `title="Cette liste a été archivée et ne peut plus être modifiée."` : `title="Modifier la liste ou/et ajouter des éléments."`}>
                                        <i class="far fa-edit"></i>
                                    </button>
                                </td>
                            </tr>`;
            }

            $("#shareForMe table tbody").innerHTML = content;
            jQuery(".t-tip").tooltip();
        } else {
            $("#shareForMe table").classList.add("d-none");
            $("#shareForMe h5").classList.remove("d-none");
        }
    }

    async openViewItemsModal(listName, id) {
        $("#shareViewList .modal-title").innerText = listName;

        let records = await this.modelItem.getItemOfList(id);

        let content = "";
        for (let item of records) {
            content += `<tr>
                            <td>${item.label}</td>
                            <td>${item.quantity}</td>
                            <td>${item.checked ? 'Oui' : 'Non'}</td>
                        </tr>`;
        }
        $("#shareViewList table tbody").innerHTML = content;
        jQuery("#shareViewList").modal("show");
    }
}

window.shareListController = new ShareListController();