class Item {

    constructor(id, label, quantity, checked, list) {
        this.id = id;
        this.label = label;
        this.quantity = quantity;
        this.checked = checked;
        this.list = list;
    }
}

class List {

    constructor(id, shop, date, archived) {
        this.id = id;
        this.shop = shop;
        this.date = date;
        this.archived = archived;
    }
}

class User {

    constructor(id, name, email, password, active, admin, blocked, subscriber) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.active = active;
        this.admin = admin;
        this.blocked = blocked;
        this.subscriber = subscriber;
    }
}