class ModelItem extends Model{
    constructor() {
        super();
        this.class = Item;
        this.api = new ItemApi();
    }

    async getItemOfList(listId) {
        return await this.api.getItemOfList(listId);
    }
}