class ModelList extends Model{
    constructor() {
        super();
        this.class = List;
        this.api = new ListApi();
    }

    async getAllList() {
        return this.formatData(await this.api.getAllList());
    }

    async getArchivedList() {
        return this.formatData(await this.api.getArchivedList());
    }

    async update(obj) {
        obj.date = toDbDate(obj.date);
        return super.update(obj);
    }

    getNoOfCurrentList() {
        return this.api.getNoOfCurrentList()
    }
}
