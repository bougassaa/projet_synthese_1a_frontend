class Model {
    constructor() {
        // initialize class && api in child class
    }

    save(obj) {
        return this.api.insert(obj)
            .then(response => response.text())
            .then(body => body);
    }

    delete(id) {
        return this.api.delete(id).then(res => res.status);
    }

    async getById(id) {
        try {
            return Object.assign(new this.class(), await this.api.getById(id));
        } catch (e) {
            if (e === 404) return null;
            return undefined
        }
    }

    update(obj) {
        return this.api.update(obj);
    }

    formatData(records) {
        let tab = [];
        for (let record of records) {
            record.date = new Date(record.date).toLocaleDateString();
            tab.push(Object.assign(new this.class(), record))
        }
        return tab;
    }
}