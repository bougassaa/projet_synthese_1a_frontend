class ModelShare extends Model{
    constructor() {
        super();
        this.api = new ShareApi();
    }

    async share(sharingList, sharingUser, canEdit) {
        return this.api.share(sharingList, sharingUser, canEdit);
    }

    async listSharedFromUser() {
        return this.api.listSharedFromUser();
    }

    async listSharedForOthers() {
        return this.api.listSharedForOthers();
    }
}