class ModelUser extends Model{
    constructor() {
        super();
        this.class = User;
        this.api = new UserApi();
    }

    async getUsersFromTyping(name) {
        return this.api.getUsersFromTyping(name);
    }

    async getSubscriber() {
        return this.api.getSubscriber(name);
    }

    changeEmail(user, email) {
        return this.api.changeEmail(user, email);
    }

    getUser() {
        return this.api.getUser();
    }

    authenticate(email, password) {
        return this.api.authenticate(email, password);
    }

    updateFromAdmin(user) {
        return this.api.updateFromAdmin(user);
    }
}