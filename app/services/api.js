class API {
    constructor() {
        this.serviceBaseUrl = "http://localhost:3333/";
        this.token = sessionStorage.getItem("token");
    }

    insert(obj) {
        return fetch(this.serviceBaseUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify(obj)
        })
    }

    delete(id) {
        return fetch(`${this.serviceBaseUrl}/${id}`, {
            method: 'DELETE' ,
            headers: {
                'Authorization': `Bearer ${this.token}`
            }
        })
    }

    getById(id) {
        return fetchJSON(`${this.serviceBaseUrl}/${id}`, this.token);
    }

    update(obj) {
        return fetch(this.serviceBaseUrl, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify(obj)
        })
    }
}
