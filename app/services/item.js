class ItemApi extends API {
    constructor() {
        super();

        this.serviceBaseUrl += `item`;
    }

    getItemOfList(listId) {
        return fetchJSON(`${this.serviceBaseUrl}/list/${listId}`, this.token);
    }
}