class ListApi extends API {
    constructor() {
        super();

        this.serviceBaseUrl += `list`;
    }

    getAllList() {
        return fetchJSON(this.serviceBaseUrl, this.token);
    }

    getArchivedList() {
        return fetchJSON(`${this.serviceBaseUrl}/archived`, this.token);
    }

    getNoOfCurrentList() {
        return fetchJSON(`${this.serviceBaseUrl}/noOfCurrentList`, this.token);
    }
}