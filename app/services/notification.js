class NotificationApi extends API {
    constructor() {
        super();

        this.serviceBaseUrl += `notification`;
    }

    sendNotification(idUser, title, message) {
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.token}`
            },
            body: JSON.stringify({"title" : title, "message" : message, "user" : idUser})
        }).then(res => {
            if (res.status === 200) {
                resolve()
            } else {
                reject(res.status)
            }
        }).catch(err => reject(err)))
    }

    getUnreadNotification(){
        return fetchJSON(`${this.serviceBaseUrl}/unread`, this.token);
    }

    getAll(){
        return fetchJSON(`${this.serviceBaseUrl}`, this.token);
    }

    checkNotification(id){
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/${id}`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${this.token}`
            }
        }).then(res => {
            if (res.status === 200) {
                resolve()
            } else {
                reject(res.status)
            }
        }).catch(err => reject(err)))
    }
}