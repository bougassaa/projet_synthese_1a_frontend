class ShareApi extends API {
    constructor() {
        super();

        this.serviceBaseUrl += `share`;
    }

    share(sharingList, sharingUser, canEdit) {
        return fetch(`${this.serviceBaseUrl}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify({
                "list" : sharingList,
                "user" : sharingUser,
                "edit" : canEdit
            })
        })
    }

    listSharedFromUser(){
        return fetchJSON(`${this.serviceBaseUrl}/fromuser`, this.token);
    }

    listSharedForOthers(){
        return fetchJSON(`${this.serviceBaseUrl}/forothers`, this.token);
    }
}