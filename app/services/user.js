class UserApi extends API {
    constructor() {
        super();
        this.serviceBaseUrl += `user`;
    }

    authenticate(email, password) {
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/authenticate`, {
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            body: `email=${email}&password=${password}`
        }).then(res => {
            if (res.status === 200) {
                resolve(res.json())
            } else {
                reject(res.status)
            }
        }).catch(err => reject(err)))
    }

    create(email, name, password) {
        let baselink = this.getBaseLinkForActivation();
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/create`, {
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            body: `email=${email}&name=${name}&password=${password}&baselink=${baselink}`
        }).then(res => {
            if (res.status === 200) {
                resolve()
            } else {
                reject(res.status)
            }
        }).catch(err => reject(err)))
    }

    checkForActivation(id) {
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/actived/${id}`)
            .then(res => {
                if (res.status === 200) {
                    resolve(res.json())
                } else {
                    reject(res.status)
                }
            })
            .catch(err => reject(err)))
    }

    checkForChangePassword(id) {
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/checkPasswordLink/${id}`)
            .then(res => {
                if (res.status === 200) {
                    resolve()
                } else {
                    reject(res.status)
                }
            })
            .catch(err => reject(err)))
    }

    changePassword(pwd, id) {
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/changePasswordFromLink`, {
            method: "PUT",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            body: `id=${id}&password=${pwd}`
        }).then(res => {
            if (res.status === 200) {
                resolve(res.json())
            } else {
                reject(res.status)
            }
        }).catch(err => reject(err)))
    }

    changeEmail(user, email) {
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/changeEmail`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Bearer ${this.token}`
            },
            body: `user=${user}&email=${email}`
        }).then(res => {
            if (res.status === 200) {
                resolve()
            } else {
                reject(res.status)
            }
        }).catch(err => reject(err)))
    }

    async getUsersFromTyping(name) {
        let res = await fetch(`${this.serviceBaseUrl}/typing`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Bearer ${this.token}`
            },
            body: `typing=${name}`
        });

        if(res.status === 200) {
            return res.json();
        } else {
            console.log("error : getUsersFromTyping");
            return false;
        }
    }

    getBaseLinkForActivation() {
        return document.URL.substr(0,document.URL.lastIndexOf('/')) + "/login.html"
    }

    newActivationFromIdActivation(id) {
        let baselink = this.getBaseLinkForActivation();
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/newActivationId`,{
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                body: `id=${id}&baselink=${baselink}`
            })
            .then(res => {
                if (res.status === 200) {
                    resolve(res.json())
                } else {
                    reject(res.status)
                }
            })
            .catch(err => reject(err)))
    }

    newActivationFromEmail(email) {
        let baselink = this.getBaseLinkForActivation();
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/newActivationEmail`,{
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            body: `email=${email}&baselink=${baselink}`
        })
            .then(res => {
                if (res.status === 200) {
                    resolve(res.json())
                } else {
                    reject(res.status)
                }
            })
            .catch(err => reject(err)))
    }

    newLinkToChangePassword(email) {
        let baselink = this.getBaseLinkForActivation();
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/linkChangePassword`,{
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            body: `email=${email}&baselink=${baselink}`
        })
            .then(res => {
                if (res.status === 200) {
                    resolve(res.json())
                } else {
                    reject(res.status)
                }
            })
            .catch(err => reject(err)))
    }

    getUser() {
        return fetchJSON(`${this.serviceBaseUrl}`, this.token);
    }

    updateFromAdmin(user) {
        return fetch(`${this.serviceBaseUrl}/admin`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify(user)
        })
    }

    checkIfSubscriber() {
        return fetchJSON(`${this.serviceBaseUrl}/checkIfSubscriber`, this.token);
    }

    subscribe(formBody) {
        return new Promise((resolve, reject) => fetch(`${this.serviceBaseUrl}/subscribe`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: `Bearer ${this.token}`
            },
            body: formBody
        }).then(res => {
            if (res.status === 200) {
                resolve()
            } else {
                reject(res.status)
            }
        }).catch(err => reject(err)))
    }

    getSubscriber() {
        return fetchJSON(`${this.serviceBaseUrl}/subscriber`, this.token);
    }
}