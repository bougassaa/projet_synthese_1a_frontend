function $(selector, f) {
    if (f == undefined)
        return document.querySelector(selector)
    else 
        document.querySelectorAll(selector).forEach(f)
}

function fetchJSON(url, token) {
    const headers = new Headers();
    if (token !== undefined) {
        headers.append("Authorization", `Bearer ${token}`)
    }
    return new Promise((resolve, reject) => fetch(url, {cache: "no-cache", headers: headers})
        .then(res => {
            if (res.status === 200) {
                resolve(res.json())
            } else {
                reject(res.status)
            }
        })
        .catch(err => reject(err)))
}

function include(selector, url, urlcontroller) {
    fetch(url, {cache: "no-cache"})
        .then(res => res.text())
        .then(html => {
            $(`#${selector}`).innerHTML = html
            fetch (urlcontroller, {cache: "no-cache"})
                .then(res => res.text())
                .then(js => {
                    eval(js)
                })
        })
        .catch(function(err) {
            console.log('Failed to fetch page: ', err)
        });
}

function navigate(view) {
    include('content',  `views/${view}.html`, `app/controllers/${view}.js`)
}

const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;
function reviver(key, value) {
    if (typeof value === "string" && dateFormat.test(value)) {
        return new Date(value);
    }
    return value;
}

function getParameterByName(name) {
    let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function setActiveLink(id) {
    $(".nav-item", item => {
        item.classList.remove('active');
    });

    if(id) {
        $(id).classList.add('active');
    }
}

function toDbDate(pDate) {
    let date_ob = new Date(pDate);

    // adjust 0 before single digit date
    let date = ("0" + date_ob.getDate()).slice(-2);

    // current month
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

    // current year
    let year = date_ob.getFullYear();

    // prints date in YYYY-MM-DD format
    return year + "-" + month + "-" + date;
}

function eventEnterKey(key, input) {
    if(key === 13) {
        if(input.dataset.forEdit == 1) {
            $('#editItem').click()
        } else {
            $('#addNewItem').click()
        }
    }
}

function validateEmail (emailAdress){
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return emailAdress.match(regexEmail);
}

function formToUrl(form) {
    let tab = [];
    let elements = form.querySelectorAll("input, select, textarea");

    for(let elem of elements) {
        let name = elem.name;
        let value = elem.value;

        if(name) {
            tab.push(`${name}=${value}`);
        }
    }

    return tab.join('&');
}